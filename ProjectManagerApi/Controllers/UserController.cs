﻿using BusinessEntities;
using ProjMgmt.BLL;
using System.Collections.Generic;
using System.Web.Http;

namespace ProjectManagerApi.Controllers
{
    /// <summary>
    /// Controller for User Model
    /// </summary>
    public class UserController : ApiController
    {
        private readonly UserBusiness oUserBusines;
        
        public UserController()
        {
            oUserBusines = new UserBusiness();
        }

        [HttpGet]
        [Route("api/getAllUsers")]
        public IEnumerable<UserModel> Get()
        {
          return  oUserBusines.GetAllUsers();
        }



        [HttpPost]
        [Route("api/updateUser")]
        public UserUpdateResult Post( UserModel oUser)
        {
            return oUserBusines.UpdateUser(oUser);
            
        }

        [HttpPost]
        [Route("api/deleteUser")]
        public Status DeleteUser(UserModel oUser)
        {
            return oUserBusines.DeleteUser(oUser);
        }
    }
}