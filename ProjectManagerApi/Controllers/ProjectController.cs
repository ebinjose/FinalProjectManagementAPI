﻿using BusinessEntities;
using ProjMgmt.BLL;
using System.Collections.Generic;
using System.Web.Http;
namespace ProjectManagerApi.Controllers
{
    /// <summary>
    /// Controller for Project Model
    /// </summary>
    public class ProjectController : ApiController
    {
        /// <summary>
        /// Instance of business layer class 
        /// </summary>
        private readonly ProjectBusiness oProject;

        /// <summary>
        /// Constructor of the Controller
        /// </summary>
        public ProjectController()
        {
            oProject = new ProjectBusiness();
        }

        /// <summary>
        /// Gets the list of all projects
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/getAllProjects")]
        public IEnumerable<ProjectModel> Get()
        {
            return oProject.GetAllProject();
        }

        /// <summary>
        /// Updates the details of a given project
        /// </summary>
        /// <param name="oProj">Object of the project</param>
        /// <returns>Object containing the status of result</returns>
        [HttpPost]
        [Route("api/updateProject")]
        public ProjectUpdateResult Post(ProjectModel oProj)
        {
            return oProject.UpdateProject(oProj);
        }
    }
}