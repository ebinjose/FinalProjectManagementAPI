﻿using BusinessEntities;
using ProjMgmt.BLL;
using System.Collections.Generic;
using System.Web.Http;
namespace ProjectManagerApi.Controllers
{
    /// <summary>
    /// Controller for Task Model
    /// </summary>
    public class TaskController : ApiController
    {
        /// <summary>
        /// Instance of the business layer
        /// </summary>
        private readonly TaskBusiness oTaskBusines;

        /// <summary>
        /// Constructor of the Controller
        /// </summary>
        public TaskController()
        {
            oTaskBusines = new TaskBusiness();
        }

        /// <summary>
        /// Get the list of all tasks
        /// </summary>
        /// <returns>List of TaskModel object</returns>
        [HttpGet]
        [Route("api/getAllTasks")]
        public IEnumerable<TaskModel> Get()
        {
            return oTaskBusines.GetAllTasks();
        }

        /// <summary>
        /// Gets list of Parent Tasks
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/getAllParentTasks")]
        public IHttpActionResult GetParentTasks()
        {
            return Ok(oTaskBusines.GetAllParentTasks());
        }

        /// <summary>
        /// Updates a given task
        /// </summary>
        /// <param name="oTask"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/updateTask")]
        public TaskUpdateResult Post(TaskModel oTask)
        {
            return oTaskBusines.UpdateTask(oTask);
        }
    }
}
