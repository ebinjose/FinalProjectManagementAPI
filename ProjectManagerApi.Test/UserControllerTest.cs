﻿using BusinessEntities;
using Newtonsoft.Json;
using NUnit.Framework;
using ProjectManagerApi.Controllers;
using System.Collections;
using System.IO;
using System.Reflection;

namespace ProjectManagerApi.Test
{
    [TestFixture]
    public class UserControllerTest
    {
        public static IEnumerable GetTestDataUser
        {
            get
            {
                string FileLoc = @"TestData\User.json";
                string FilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace("file:\\","").Replace("\\bin\\Debug", "");

                var jsonText = File.ReadAllText(Path.Combine(FilePath,FileLoc));
                var adduser = JsonConvert.DeserializeObject<UserModel>(jsonText);
                yield return adduser;
            }
        }
        
        [Test, TestCaseSource("GetTestDataUser")]
        public void TestAddUser(UserModel testUser)
        {
            UserController oController = new UserController();
            UserUpdateResult uResult = new UserUpdateResult();
            uResult = oController.Post(testUser);
            string message = uResult.status.Message;
            Assert.AreEqual("User added successfully", message);
            testUser.User_ID = uResult.user.User_ID;
            Assert.AreEqual("User updated successfully",oController.Post(testUser).status.Message);
            Assert.NotNull(oController.Get());
            Assert.IsTrue(oController.DeleteUser(testUser).Result);

        }
         
    }
}
